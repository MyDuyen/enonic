var gulp = require('gulp');
var sass = require('gulp-sass');
var wait = require('gulp-wait');
var livereload = require('gulp-livereload');

gulp.task('sass', function () {
    return gulp.src('src/main/resources/assets/scss/style.scss')
        .pipe(wait(1500))
        .pipe(sass({outputStyle: 'compressed'}))    
        .on('error', onError)
        .pipe(gulp.dest('src/main/resources/assets'))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('src/main/resources/assets/scss/**/*', ['sass']);
});

gulp.task('default', ['sass', 'watch']);

function onError (error) {
    // If you want details of the error in the console
    console.log(error.toString())
    this.emit('end')
}