$(function(){
	function menuResponsive(){
		$("#menu-special").click(function(){
			$(".menu-special-overlay").css('display', 'block');
			$("body").css('overflow','hidden');
		});
	
		$("#menu-special-close").click(function(){
			$(".menu-special-overlay").css('display', 'none');
			$("body").css('overflow','unset');
		});
	
		$(window).resize(function() {
			var width = $(window).width();
			if (width > 1023){
				$(".menu-special-overlay").css('display', 'none');
			}
		});
	}

	function search(){
		$(".search").click(function(e){
			if($(".area-search").css('display') == 'none'){
				$(".area-search").css('display', 'block');
				$("#s").focus();
			}
			else{
				$(".area-search").slideUp("fast");
			}
		});
	
		$(document).mouseup(function (e){
	
			var container = $(".area-search");
		
			if (!container.is(e.target) && container.has(e.target).length === 0){
				container.slideUp("fast");		
			}
		}); 
	}


	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	}

	function translate(){
		$("#english").on("click",function(e){
			e.preventDefault();
			var pathname = window.location.pathname;
			if(pathname.indexOf('/en') <= -1 ){
				pathname = pathname.replace('/not-as','/not-as/en');
			}
			var number = getUrlVars()["s"];
			console.log(number);
			if(number !== undefined){
				pathname = pathname + '?s=' + number;
			}
			window.location.replace(pathname);
	
		});
	
		$("#norway").on("click",function(e){
			e.preventDefault();
			var pathname = window.location.pathname;
			pathname =  pathname.replace('/not-as/en','/not-as');
			var number = getUrlVars()["s"];
			console.log(number);
			if(number !== undefined){
				pathname = pathname + '?s=' + number;
			}
			window.location.replace(pathname);
		});
	}

	function scrollTop(){

		$(window).scroll(function(){
			if($(window).scrollTop() > 300){
				$('.gototop').css('visibility','visible');
				$('.gototop').css('opacity','0.7');
			}
			else{
				$('.gototop').css('visibility','hidden');
				$('.gototop').css('opacity','0');
			}
		});
		$('.gototop').click(function(event) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: $("body").offset().top
			}, 500);
	   });
	}


	if(getUrlVars()["s"] != undefined){
		$("#s").val(getUrlVars()["s"]);
	}

	menuResponsive();
	search();
	translate();
	scrollTop();
});