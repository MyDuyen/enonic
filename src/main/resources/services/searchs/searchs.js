var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library

exports.post = function(req) {
    var content = portal.getContent();

	var records_per_page = req.params.records_per_page;
	var current_page = req.params.current_page;
    var data = req.params.data;
    var pathname = content._path;
    var language = '';
    if(pathname.indexOf('/en') > -1 ){
            language = 'en';
    }
    else{
        language = 'no';
    }

	var list_search_results = [];

    var result = contentLib.query({
        start: records_per_page*current_page,
        count: current_page,
		contentTypes: [
			app.name + ':page',
			app.name + ':post'
        ],
		"query": "fulltext('displayName','" + data + "','OR') AND language='"+ language + "'"
	});

	
	var hits = result.hits;

	for(var i = 0; i < hits.length; i++) {

        var list_search_result = {};
        list_search_result.name = hits[i].displayName;
        list_search_result.contentUrl = portal.pageUrl({
            id: hits[i]._id
		});
		list_search_result.time = hits[i].createdTime.slice(0,10).replace(/-/g,"-");

        list_search_result.path = list_search_result.contentUrl.slice(14);
        list_search_results.push(list_search_result);
	}
	
	return {
		body: list_search_results,
		contentType: 'application/json'
	};

};
