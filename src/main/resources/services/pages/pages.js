var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library

exports.post = function(req) {
    var site = portal.getSite();
    var content = portal.getContent();

	var records_per_page = req.params.records_per_page;
	var current_page = req.params.current_page;
    // var language = req.params.language;
    
    var pathname = content._path;
    var language = '';
    if(pathname.indexOf('/en') > -1 ){
            language = 'en';
    }
    else{
        language = 'no';
    }

	var posts = [];

    var resultPost = contentLib.query({
        start: records_per_page*current_page,
        count: current_page,
        contentTypes: [
            app.name + ':post'
        ],
        "query": "_path LIKE '/content" + site._path + "/*' AND language='"+ language + "'",
        sort: "modifiedTime ASC"
	});

	
	var hitsPost = resultPost.hits;

	for(var i = 0; i < hitsPost.length; i++) {

        var post = {};
        post.name = hitsPost[i].displayName;
        post.contentUrl = portal.pageUrl({
            id: hitsPost[i]._id
        });

        post.path = post.contentUrl.slice(14);
        posts.push(post);
	}
	
	return {
		body: posts,
		contentType: 'application/json'
	};

};
