var portalLib = require('/lib/xp/portal');
// var adminLib = require('/lib/xp/admin');
var thymeleaf = require('/lib/xp/thymeleaf');

exports.get = function (req) {
    var view = resolve('mytool.html');

    // var params = {
    //     adminUrl: adminLib.getBaseUri(),
    //     assetsUrl: portalLib.assetUrl({
    //         path: ''
    //     }),
    //     launcherPath: adminLib.getLauncherPath(),
	// 	launcherUrl: adminLib.getLauncherUrl(),
	// 	// launcherUrl: portalLib.assetUrl({
    //     //     path: '/js/launcher',
    //     //     application: 'com.enonic.xp.app.main'
    //     // }),
	// 	appName: 'my-custom-tool'
    // };

    return {
        body: thymeleaf.render(view, {})
    };
};