var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library
var util = require('/lib/enonic/util');
// var menu = require('/lib/enonic/menu');

// Handle the GET request

exports.get = function(req) {
    var site = portal.getSite();
    var siteConfig = portal.getSiteConfig();
    var content = portal.getContent();

    var pathImg = siteConfig.imageLogo;

    var pathname = content._path;
    var pathHome = '';

    if(pathname.indexOf('/en') > -1 ){
        pathHome = '/not-as/en'
    }
    else{
        pathHome = '/not-as'
    }

    // var menuItems = menu.getMenuTree(2);

    var pathimg = portal.imageUrl({
        id: pathImg,
        scale: 'width(142)'
    })
    // Extract the main region which contains component parts
    var mainRegion = content.page.regions.main;

    // Add the menu data to the model
    var model = {
        menuItems : getMenuTree(3),
        pathimg: pathimg,
        mainRegion: mainRegion,
        pathContent: content._path,
        pathSearch: portal.pageUrl({
            path: site._path + '/search'
        }) ,
        pathHome: portal.pageUrl({
            path: pathHome
        }),
        pathHomeShort: portal.pageUrl({
            path: pathHome
        }).slice(14),
        name: siteConfig.companyName,
        website: siteConfig.companyWebsite,
        email: siteConfig.companyEmail,
    };


    // Specify the view file to use
    var view = resolve('page.html');


    // Return the merged view and model in the response object
    return {
        body: thymeleaf.render(view, model)
    }
    
};
var globals = {
    appPath: util.app.getJsonName()
};

var getMenuTree = function (levels) {
    var menu = [];
    var site = portal.getSite();

    if (site) {
        menu = doGetSubMenus(site, levels);
    }

    return menu;
};

var doGetSubMenus = function (parentContent, levels) {
    var subMenus = [];
    var currentContent = portal.getContent() || parentContent;

    if (parentContent.type === 'portal:site' && isMenuItem(parentContent)) {
        subMenus.push(renderMenuItem(currentContent, parentContent, 0));
    }

    var children = getChildMenuItems(parentContent);

    levels--;

    var loopLength = children.hits.length;
    for (var i = 0; i < loopLength; i++) {
        var child = children.hits[i];
        if (isMenuItem(child)) {
            subMenus.push(renderMenuItem(currentContent, child, levels));
        }
    }

    return subMenus;
};

var getChildMenuItems = function (parent) {

    return contentLib.query({
        count: 1000,
        sort: parent.childOrder,
        filters: {
            boolean: {
                must: [
                    {
                        hasValue: {
                            field: "_parentPath",
                            values: [
                                "/content" + parent._path
                            ]
                        }
                    }
                    ,
                    {
                        hasValue: {
                            field: "x." + globals.appPath + ".menu-item.menuItem",
                            values: [
                                "true"
                            ]
                        }
                    }
                ]
            }
        }
    });
};

var isMenuItem = function(content) {
    var extraData = content.x;
    if (!extraData) {
        return false;
    }
    var extraDataModule = extraData[globals.appPath];
    if (!extraDataModule || !extraDataModule['menu-item']) {
        return false;
    }
    var menuItemMetadata = extraDataModule['menu-item'] || {};

    return menuItemMetadata['menuItem'];
}

var renderMenuItem = function(currentContent, content, levels) {
    var subMenus = [];
    if (levels > 0) {
        subMenus = doGetSubMenus(content, levels);
    }

    var inPath = false;
    var isActive = false;

    if (content._path === currentContent._path.substring(0, content._path.length)) {
        inPath = true;
    }

    if (content._path === currentContent._path) {
        isActive = true;
    }

    var menuItem = content.x[globals.appPath]['menu-item'];

    return {
        displayName: content.displayName,
        menuName: menuItem.menuName && menuItem.menuName.length ? menuItem.menuName : null,
        path: content._path,
        hasChildren: subMenus.length > 0,
        inPath: inPath,
        isActive: isActive,
        children: subMenus,
    };
}



