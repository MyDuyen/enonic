var portal = require('/lib/xp/portal');
var thymeleaf = require('/lib/xp/thymeleaf');

exports.get = function(req) {

  // Find the current component.
  var component = portal.getComponent();

  // Define the model
  var model = {
    leftRegion: component.regions["left"],
    rightRegion: component.regions["right"],
    classname: component.config.classname || ''
  };

  // Resolve the view
  var view = resolve('layout-two-intro.html');

  // Render a thymeleaf template
  var body = thymeleaf.render(view, model);

  // Return the result
  return {
    body: body,
    contentType: 'text/html'
  };

};