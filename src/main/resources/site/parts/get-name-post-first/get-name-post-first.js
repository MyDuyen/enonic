var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library

// Handle the GET request
exports.get = function(req) {

    var site = portal.getSite();
    var content = portal.getContent();
	var pathname = content._path;
	var language = '';
    if(pathname.indexOf('/en') > -1 ){
        language = 'en';
    }
    else{
        language = 'no';
	}
    var resultPost = contentLib.query({
        start: 0,
        count: 100,
        contentTypes: [
            app.name + ':post'
        ],
        "query": "_path LIKE '/content" + site._path + "/*' AND language='"+ language + "'",
        sort: "modifiedTime ASC"
    });

    
    var hitsPost = resultPost.hits;

    var posts = [];
    


    // Loop through the contents and extract the needed data
    

    for(var i = 0; i < hitsPost.length; i++) {

        var post = {};
        post.name = hitsPost[i].displayName;
        post.contentUrl = portal.pageUrl({
            id: hitsPost[i]._id
        });

        post.path = post.contentUrl.slice(14);
        posts.push(post);
    }


    // Add the menu data to the model
    var model = {
		post_first: posts[0],
    };


    // Specify the view file to use
    var view = resolve('get-name-post-first.html');


    // Return the merged view and model in the response object
    return {
        body: thymeleaf.render(view, model)
    }
};