
var thymeleaf = require('/lib/xp/thymeleaf'); // Import the Thymeleaf library
var portal = require('/lib/xp/portal'); // Import the portal functions

// Handle the GET request
exports.get = function(req) {
    var config = portal.getComponent().config;
    var location = config.location;
    var siteConfig = portal.getSiteConfig();
    var googleApiKey = siteConfig.googleApiKey;
    // var googleMaps = '<script src="http://maps.googleapis.com/maps/api/js?key=' + googleApiKey + '"></script>' + 
    //                     '<script>'+
    //                         'function initialize() {'+
    //                             'var geocoder = new google.maps.Geocoder();'+
    //                             'var address = {};'+
    //                             'geocoder.geocode({"address": '+ location + '}, function(result, status) {'+
    //                                 'if (status == google.maps.GeocoderStatus.OK) {'+
    //                                     'var latitude = results[0].geometry.location.lat();'+
    //                                     'var longitude = results[0].geometry.location.lng();'+
    //                                     'address = {'+
    //                                         'lat: latitude,'+
    //                                         'lng: longitude'+
    //                                     '}'+
    //                                 '}'+
    //                             '});'+
    //                             'var map = new google.maps.Map(document.getElementById("map"),{zoom: 4, center: address});'+
    //                             'var marker = new google.maps.Marker({ position:address, map: map}); marker.setMap(map);'+
    //                         '} '+
    //                         'google.maps.event.addDomListener(window, "load", initialize);'+
    //                     '</script>';
    // Prepare the model object with the needed data from the content
    

    // Specify the view file to use
    var view = resolve('map.html');

    
    var model = {
    };
    // Return the merged view and model in the response object
    return {
        body: thymeleaf.render(view, model)
        // pageContributions: {
        //     headEnd: googleMaps
        // }
    }
};