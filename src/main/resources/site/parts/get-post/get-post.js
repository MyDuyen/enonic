var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library

var portal = require('/lib/xp/portal'); // Import the portal library

// Handle the GET request
exports.get = function(req) {
	var content = portal.getContent();

    var urlPost = portal.pageUrl({
        _path: content._path
    })
    // Add the menu data to the model
    var model = {
		namePost: content.displayName,
		urlPost: urlPost
    };


    // Specify the view file to use
    var view = resolve('get-post.html');


    // Return the merged view and model in the response object
    return {
        body: thymeleaf.render(view, model)
    }
};