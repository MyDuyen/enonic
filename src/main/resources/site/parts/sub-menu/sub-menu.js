var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library

// Handle the GET request
exports.get = function(req) {

    var site = portal.getSite();
    var content = portal.getContent();
    var pathname = content._path;
    var language = '';
    if(pathname.indexOf('/en') > -1 ){
            language = 'en';
    }
    else{
        language = 'no';
    }

    var resultSubMenu = contentLib.query({
        start: 0,
        count: 100,
        contentTypes: [
            app.name + ':page-sub-menu'
        ],
        "query": "_path LIKE '/content" + site._path + "/*' AND language='"+ language + "'",
        sort: "modifiedTime ASC"
    })
    
    var hitsSub = resultSubMenu.hits;

    var submenus = [];

    // Loop through the contents and extract the needed data
    

    for(var i = 0; i < hitsSub.length; i++) {

        var menu = {};
        menu.name = hitsSub[i].displayName;
        menu.contentUrl = portal.pageUrl({
            id: hitsSub[i]._id
        });
        submenus.push(menu);
    }


    // Extract the main region which contains component parts
   
    // Add the menu data to the model
    var model = {
        
        submenus: submenus,
        
    };


    // Specify the view file to use
    var view = resolve('sub-menu.html');

    // Return the merged view and model in the response object
    return {
        body: thymeleaf.render(view, model)
    }
};