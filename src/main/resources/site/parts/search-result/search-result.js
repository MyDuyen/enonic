var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content'); // Import the content service library
var portal = require('/lib/xp/portal'); // Import the portal library

// Handle the GET request

exports.get = function(req) {
	var site = portal.getSite();
	var data = req.params.s;
	var content = portal.getContent();
	var pathname = content._path;
	var language = '';
    if(pathname.indexOf('/en') > -1 ){
        language = 'en';
    }
    else{
        language = 'no';
	}
	
	var result = contentLib.query({
        start: 0,
        count: 2,
        contentTypes: [
			app.name + ':page',
			app.name + ':post'
        ],
		"query": "fulltext('displayName','" + data + "','OR') AND language='"+ language + "'",
	});

	var results = contentLib.query({
        start: 0,
        count: 100,
        contentTypes: [
			app.name + ':page',
			app.name + ':post'
        ],
		"query": "fulltext('displayName','" + data + "','OR') AND language='"+ language + "'",
	});
	
	var hits = result.hits;
	var list_search_results = [];

    // Loop through the contents and extract the needed data
    for(var i = 0; i < hits.length; i++) {

        var list_search_result = {};
        list_search_result.name = hits[i].displayName;
        list_search_result.contentUrl = portal.pageUrl({
            id: hits[i]._id
		});
		list_search_result.time = hits[i].createdTime.slice(0,10).replace(/-/g,"-");

        list_search_result.path = list_search_result.contentUrl.slice(14);
        list_search_results.push(list_search_result);
	}

	var path = portal.serviceUrl({
        service: 'searchs'
    });
	
    var model ={
		list_search_results: list_search_results,
		data: data,
		pathSearch: portal.pageUrl({
            path: site._path + '/search'
		}),
		number: results.hits.length,
		language: language,
		path: path
	};
	
    // Specify the view file to use
    var view = resolve('search-result.html');


    // Return the merged view and model in the response object
    return {
		body: thymeleaf.render(view, model)
    }
    
};


